package JavaLabs3;

/**
 * Created by Dima1 on 10.02.2016.
 * заполнить двумерный массив 7*4 случайными числами в диапозоне от -5 до 5,вывести массив на экран,
 * найти строку с максимальным по модулю произведением элементов строки и вывести номер этой строки
 */
public class Nomer5 {
    public static void main(String[] args) {
        int[][] mas = new int[7][4];
        //две переменные imod-максимальное произведение элементов в строке,к-индекс строки
        int imod = 0, k = 0;
        for (int i = 0; i < 7; i++) {
            System.out.println();
            //переменная для счёта произведения
            int mod = 1;
            for (int j = 0; j < 4; j++) {
                mas[i][j] = (int) (Math.random() * 11) - 5;
                System.out.print(mas[i][j] + "\t");
                mod = mod * mas[i][j];
            }
            if (Math.abs(mod) > imod) {
                imod = mod;
                k = i;
            }
        }
        System.out.println("\nИндекс строки с наибольшим по модулю произведением элементов " + k);
    }
}
