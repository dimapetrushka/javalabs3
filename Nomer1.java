package JavaLabs3;

/**
 * Created by Dima1 on 09.02.2016.
 * Вывести массив чётных числе от 2 до 20
 * Сначало в строку, потом в столбик
 */
public class Nomer1 {
    public static void main(String[] args) {
        //заполняем массив
        int[] mas = new int[]{2, 4, 6, 8, 10, 12, 14, 16, 18, 20};
        //выводим массив в сторку
        for (int i = 0; i < 10; i++) {
            System.out.print(mas[i] + " ");
        }
        System.out.println();
        //выводим массив в столбик
        for (int i = 0; i < 10; i++) {
            System.out.println(mas[i]);
        }
    }
}
