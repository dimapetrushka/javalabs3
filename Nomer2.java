package JavaLabs3;

/**
 * Created by Dima1 on 09.02.2016.
 * вывести массив нечётных чисел от 1 до 99
 * сначало в прямом порядке в строку, а потом в обратном порядке в строку
 */
public class Nomer2 {
    public static void main(String[] args) {
        int[] mas = new int[50];
        for (int i = 0; i < 50; i++) {
            mas[i] = i * 2 + 1;
            System.out.print(mas[i] + " ");
        }
        System.out.println();
        for (int i = 49; i >= 0; i--) {
            System.out.print(mas[i] + " ");
        }
    }
}
