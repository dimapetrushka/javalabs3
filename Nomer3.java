package JavaLabs3;

/**
 * Created by Dima1 on 09.02.2016.
 * заполнить массив из 15 элементов,вывести массив,вывести количество чётных элементов в массиве
 */
public class Nomer3 {
    public static void main(String[] args) {
        int[] mas = new int[15];
        int j = 0;
        for (int i = 0; i < 15; i++) {
            //заполняем массив вызывая функцию псевдослучайных чисел
            mas[i] = (int) (Math.random() * 10);
            System.out.print(mas[i] + " ");
            if (mas[i] % 2 == 0) j++;
        }
        System.out.println("\nЧисло чётных чисел в массиве " + j);
    }
}
